var Procentric = require('../index');
var $ = require('jquery');
window.jQuery = window.$ = $;

window.PelicanDevice = new Procentric();
document.addEventListener('deviceready', onDeviceReady, false);
document.addEventListener('deviceprogress', onDeviceProgress, false);
document.addEventListener('devicefail', onDeviceFail, false);

$(function(){
    window.PelicanDevice.bootstrap();
});

function onDeviceReady(e) {
    $('<li/>').text("Procentric is ready - Don't worry love, cavalry's here!").appendTo('#messages');
    console.log('Procentric ready');
}

function onDeviceProgress(e) {
    $('<li/>').text('Progress: ' + e.data).appendTo('#messages');
}

function onDeviceFail(e) {
    $('<li/>').text('Fail: ' + e.data).appendTo('#messages');
}
