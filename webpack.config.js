var webpack = require('webpack');
var path = require('path');
var nodeExternals = require('webpack-node-externals');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, '');

var config = {
    debug: true,
    devtool: 'source-map',

    context: APP_DIR,

    entry: [
        './index.js'
    ],

    output: {
        filename: 'pelican-procentric.min.js',
        path: BUILD_DIR,
    },

    module: {
        loaders: [
            {test: /\.js(x)?$/, exclude: /(node_modules|vendors)/, loaders: ['babel']}
        ]
    },

    target: 'node',
    externals: [
        nodeExternals()
    ]
};

module.exports = config;