var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'tests');
var APP_DIR = path.resolve(__dirname, '');

var config = {
    debug: true,
    devtool: 'source-map',

    context: APP_DIR,

    entry: [
        './tests/test.js'
    ],

    output: {
        filename: 'pelican-procentric.test.js',
        path: BUILD_DIR,
    },

    module: {
        loaders: [
            {test: /\.js(x)?$/, exclude: /(node_modules|vendors)/, loaders: ['babel']}
        ]
    }
};

module.exports = config;