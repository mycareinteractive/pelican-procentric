var debug = {
    log: function() {
        var args = Array.prototype.slice.call(arguments);
        args.splice(0, 0, '[PROCENTRIC]');
        var msg = args.join(' ');
        console.log.call(console, msg);
    }
};
export default debug;