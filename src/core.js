var debug = require('./debug').default;

export default class Core {

    constructor(parent) {
        this.parent = parent;

        // cached hcap values
        this.model = "";
        this.platformVersion = "";
        this.serialNumber = "";
        this.networkDevices = [];
        this.powerState = undefined;
        this.volume = undefined;
        this.applicationList = [];
        this.startChannel = {};

        // private properties
        this.errors = [];

        // preload
        this.preload = {
            model: {
                name: "Hardware Model",
                method: this.fetchModelName,
                mandatory: true
            },
            version: {
                name: "Platform Version",
                method: this.fetchPlatformVersion,
                mandatory: true
            },
            serial: {
                name: "Serial Number",
                method: this.fetchSerialNumber,
                mandatory: true
            },
            netDeviceCount: {
                name: "Network Device Count",
                method: this.fetchNetworkDeviceCount,
                mandatory: true
            },
            power: {
                name: "Power State",
                method: this.fetchPowerState
            },
            volume: {
                name: "Volume Level",
                method: this.fetchVolumeLevel
            },
            channel: {
                name: "Start Channel",
                method: this.fetchStartChannel
            },
            appList: {
                name: "Application List",
                method: this.fetchApplicationList
            }
        };
    }

    successCallback() {
        debug.log('Procentric is Ready');
        this.parent.triggerDeviceReady();
    }

    failedCallback() {
        this.parent.triggerDeviceFail();
    }

    progressCallback() {
        this.parent.triggerDeviceProgress();
    }

    executePreload() {
        for (var p in this.preload) {
            var item = this.preload[p];
            debug.log('Procentric is preloading:');
            debug.log(p);
            item.completed = false;
            if (item.method)
                item.method.apply(this);
        }
    }

    preloadCheck() {
        var self = this;
        if (this.isPreloaded() == false) {
            setTimeout(function () {
                self.preloadCheck.apply(self);
            }, 500);
        } else {
            this.successCallback();
            debug.log('BOOTSTRAPPED Instance!');
            debug.log('Procentric initialize complete');
        }
    }

    isPreloaded() {
        for (var p in this.preload) {
            var item = this.preload[p];
            if (!item.completed && item.mandatory == true) {
                return false;
            }
        }
        return true;
    }

    setError(method, message) {
        this.errors.push({
            method: method,
            message: message
        });
    }

    fetchModelName() {
        var self = this;
        hcap.property.getProperty({
            key: "model_name",
            onSuccess: function (res) {
                var msg = "[fetchModelName SUCCESS]: We successfully retrieved the Model Name: " + res.value;
                self.model = res.value;
                self.preload.model.completed = true;
                self.progressCallback(self.preload.model.name);
                debug.log(msg);
            },
            onFailure: function (res) {
                var msg = "[fetchModelName FAIL]: We failed to retrieve the Model Name. errorMessage: " + res.errorMessage;
                self.setError('fetchModelName', res.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.model.name);
                debug.log(msg);
            }
        });
    }

    fetchNetworkDeviceCount() {
        var self = this;
        debug.log('Fetching Network Device Count');
        hcap.network.getNumberOfNetworkDevices({
            onSuccess: function (res) {
                var msg = "[fetchNetworkDeviceCount SUCCESS]: We successfully retrieved the Network Device Count: " + res.count;
                self.fetchNetworkDeviceList(res.count);
                self.preload.netDeviceCount.completed = true;
                self.progressCallback(self.preload.netDeviceCount.name);
                debug.log(msg);
            },
            onFailure: function (res) {
                var msg = "[fetchNetworkDeviceCount FAIL]: We failed to retrieved the Network Device Count. errorMessage" + res.errorMessage;
                self.setError('fetchNetworkDeviceCount', res.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.netDeviceCount.name);
                debug.log(msg);
            }
        });
    }

    fetchNetworkDeviceList(count) {
        var self = this;
        self.networkDevices = [];
        for (var i = 0; i < count; i++) {
            self.preload['interface' + i] = {
                name: 'Interface ' + i,
                completed: false,
                mandatory: true
            };
            self.fetchNetworkDevice(i);
        }
    }

    fetchNetworkDevice(index) {
        var self = this;
        debug.log('Fetching Network Device');
        hcap.network.getNetworkDevice({
            index: index,
            onSuccess: function (res) {
                var msg = "[fetchNetworkDevice SUCCESS]: We successfully retrieved the Network Device: " + res.name;
                if (typeof res == 'object') {
                    self.networkDevices.push(res);
                }
                self.preload['interface' + index].name = res.name;
                self.preload['interface' + index].completed = true;
                self.progressCallback('interface' + index + ': ' + res.name);
                debug.log(msg);
            },
            onFailure: function (res) {
                var msg = "[fetchNetworkDevice FAIL]: We failed to retrieve the Network Device. errorMessage " + res.errorMessage;
                self.setError('fetchNetworkDevice', res.errorMessage);
                self.progressCallback('FAIL: ' + self.preload['interface' + index].name);
                debug.log(msg);
            }
        });
    }

    fetchApplicationList() {
        var self = this;
        debug.log('Fetching Application List');
        hcap.preloadedApplication.getPreloadedApplicationList({
            onSuccess: function (res) {
                var msg = "[fetchApplicationList SUCCESS]: We successfully retrieved the Application List. errorMessage: ";
                self.applicationList = res.list;
                self.preload.appList.completed = true;
                self.progressCallback(self.preload.appList.name);
            },
            onFailure: function (res) {
                var msg = "[fetchApplicationList FAIL]: We failed to fetch the Application List. errorMessage: " + res.errorMessage;
                self.setError('fetchApplicationList', res.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.appList.name);
                debug.log(msg);
            }
        });
    }

    fetchStartChannel() {
        var self = this;

        hcap.channel.getStartChannel({
            onSuccess: function (s) {
                self.startChannel = s;
                self.preload.channel.completed = true;
                self.progressCallback(self.preload.channel.name);
                debug.log("[fechStartChannel SUCCESS]: Successfully retrieved Start Channel object: " +
                    "\n channel type      : " + s.channelType +
                    "\n logical number    : " + s.logicalNumber +
                    "\n frequency         : " + s.frequency +
                    "\n program number    : " + s.programNumber +
                    "\n major number      : " + s.majorNumber +
                    "\n minor number      : " + s.minorNumber +
                    "\n satellite ID      : " + s.satelliteId +
                    "\n polarization      : " + s.polarization +
                    "\n rf broadcast type : " + s.rfBroadcastType +
                    "\n ip                : " + s.ip +
                    "\n port              : " + s.port +
                    "\n ip broadcast type : " + s.ipBroadcastType +
                    "\n symbol rate       : " + s.symbolRate +
                    "\n pcr pid           : " + s.pcrPid +
                    "\n video pid         : " + s.videoPid +
                    "\n video stream type : " + s.videoStreamType +
                    "\n audio pid         : " + s.audioPid +
                    "\n audio stream type : " + s.audioStreamType +
                    "\n signal strength   : " + s.signalStrength +
                    "\n source address    : " + s.sourceAddress);
            },
            "onFailure": function (f) {
                var msg = "[fechStartChannel FAIL]: We were not able to retrieve the Start Channel: onFailure : errorMessage = " + f.errorMessage;
                self.setError('fetchStartChannel', f.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.channel.name);
                debug.log(msg);
            }
        })
    }

    fetchPlatformVersion() {
        var self = this;
        hcap.property.getProperty({
            key: "platform_version",
            onSuccess: function (res) {
                var msg = "[fetchPlatformVersion SUCCESS]: We successfully fetched the Platform Version.";
                self.platformVersion = res.value;
                self.preload.version.completed = true;
                self.progressCallback(self.preload.version.name);
                debug.log(msg);
            },
            onFailure: function (res) {
                var msg = "[fetchPlatformVersion FAIL]: We failed to fetch the Platform Version. errorMessage: " + res.errorMessage;
                self.setError('fetchPlatformVersion', res.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.version.name);
                debug.log(msg);
            }
        });
    }

    fetchSerialNumber() {
        var self = this;
        hcap.property.getProperty({
            key: "serial_number",
            onSuccess: function (res) {
                var msg = "[fetchSerialNumber SUCCESS]: We successfully fetched the serial number.";
                self.serialNumber = res.value;
                self.preload.serial.completed = true;
                self.progressCallback(self.preload.serial.name);
                debug.log(msg);
            },
            onFailure: function (res) {
                var msg = "[fetchSerialNumber FAIL]: We failed to fetch the Platform Version. errorMessage: " + res.errorMessage;
                self.setError('fetchSerialNumber', res.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.serial.name);
                debug.log(msg);
            }
        });
    }

    fetchPowerState() {
        var self = this;
        hcap.power.getPowerMode({
            "onSuccess": function (s) {
                self.powerState = s.mode;
                self.preload.power.completed = true;
                self.progressCallback(self.preload.power.name);
                debug.log("[fetchPowerState SUCCESS]: power mode " + s.mode);
            },
            "onFailure": function (f) {
                var msg = "[fetchPowerState FAIL]: Failed to retrieve power status. errorMessage = " + f.errorMessage;
                self.setError('fetchPowerState', f.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.power.name);
                debug.log(msg);
            }
        });
    }

    fetchVolumeLevel() {
        var self = this;
        hcap.volume.getVolumeLevel({
            "onSuccess": function (s) {
                self.volume = s.level;
                self.preload.volume.completed = true;
                self.progressCallback(self.preload.volume.name);
                debug.log("[fetchVolumeLevel SUCCESS]: volume level " + s.level);
            },
            "onFailure": function (f) {
                var msg = "[fetchVolumeLevel FAIL]: Failed to retrieve volume level. errorMessage = " + f.errorMessage;
                self.setError('fetchVolumeLevel', f.errorMessage);
                self.progressCallback('FAIL: ' + self.preload.volume.name);
                debug.log(msg);
            }
        });
    }

    setProperty(key, val) {
        hcap.property.setProperty({
            key: key,
            value: val,
            "onSuccess": function () {
                console.log("[setProperty]: Successfully set " + key + " = " + val);
            },
            "onFailure": function (f) {
                console.log("[setProperty]: FAILED to set property " + key + " = " + val + " [ errorMessage = " + f.errorMessage + " ]");
            }
        });
    }

    setPowerState(param) {
        hcap.power.setPowerMode({
            "mode": param,
            "onSuccess": function (s) {
                debug.log("Power mode has been set to", param);
            },
            "onFailure": function (f) {
                debug.log("Failed to set power status. errorMessage = " + f.errorMessage);
            }
        });
    }

    setVolumeLevel(param) {
        hcap.volume.setVolumeLevel({
            "level": param,
            "onSuccess": function (s) {
                debug.log("Volume level has been set to", param);
            },
            "onFailure": function (f) {
                debug.log("Failed to set volume level. errorMessage = " + f.errorMessage);
            }
        });
    }

    setLocalTime(param) {
        hcap.time.setLocalTime({
            year: param.year,
            month: param.month,
            day: param.day,
            hour: param.hour,
            minute: param.minute,
            second: param.second,
            gmtOffsetInMinute: param.gmtOffsetInMinute,
            isDaylightSaving: param.isDaylightSaving,
            onSuccess: function () {
                debug.log('Local time successfully set on the device', JSON.stringify(param));
            },
            onFailure: function () {
                debug.log('FAILED to set the local time on the device');
            }
        });
    }
}
    
    