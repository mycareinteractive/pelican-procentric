var KeyCodes = require('pelican-device').KeyCodes;
var debug = require('./debug').default;

export default class KeyHandler {

    constructor(procentric) {
        this.procentric = procentric;
    }

    process(e) {
        debug.log('PROCENTRIC keydown, code', e.which);
        var key = keyMap[e.which];
        if (key) {
            this.procentric.triggerKey(e, key);
        }
    }
}

const keyMap = {};

keyMap[hcap.key.Code.BACK] = KeyCodes.Back;
keyMap[hcap.key.Code.EXIT] = KeyCodes.Exit;
keyMap[hcap.key.Code.ENTER] = KeyCodes.Enter;
keyMap[hcap.key.Code.PAUSE] = KeyCodes.Pause;
keyMap[hcap.key.Code.PAGE_UP] = KeyCodes.PageUp;
keyMap[hcap.key.Code.PAGE_DOWN] = KeyCodes.PageDown;
keyMap[hcap.key.Code.LEFT] = KeyCodes.Left;
keyMap[hcap.key.Code.UP] = KeyCodes.Up;
keyMap[hcap.key.Code.RIGHT] = KeyCodes.Right;
keyMap[hcap.key.Code.DOWN] = KeyCodes.Down;
keyMap[hcap.key.Code.NUM_0] = KeyCodes.Num0;
keyMap[hcap.key.Code.NUM_1] = KeyCodes.Num1;
keyMap[hcap.key.Code.NUM_2] = KeyCodes.Num2;
keyMap[hcap.key.Code.NUM_3] = KeyCodes.Num3;
keyMap[hcap.key.Code.NUM_4] = KeyCodes.Num4;
keyMap[hcap.key.Code.NUM_5] = KeyCodes.Num5;
keyMap[hcap.key.Code.NUM_6] = KeyCodes.Num6;
keyMap[hcap.key.Code.NUM_7] = KeyCodes.Num7;
keyMap[hcap.key.Code.NUM_8] = KeyCodes.Num8;
keyMap[hcap.key.Code.NUM_9] = KeyCodes.Num9;
keyMap[hcap.key.Code.PORTAL] = KeyCodes.Menu;   // Portal Key is mapped to MENU on pillow speaker
keyMap[hcap.key.Code.CH_DOWN] = KeyCodes.ChDown;
keyMap[hcap.key.Code.CH_UP] = KeyCodes.ChUp;
keyMap[hcap.key.Code.REWIND] = KeyCodes.Rewind;
keyMap[hcap.key.Code.PLAY] = KeyCodes.Play;
keyMap[hcap.key.Code.GUIDE] = KeyCodes.Play;    // Guide is mapped as Play/pause
keyMap[hcap.key.Code.FAST_FORWARD] = KeyCodes.Forward;
keyMap[hcap.key.Code.STOP] = KeyCodes.Stop;
keyMap[hcap.key.Code.MUTE] = KeyCodes.Mute;
keyMap[hcap.key.Code.VOL_DOWN] = KeyCodes.VolumeDown;
keyMap[hcap.key.Code.VOL_UP] = KeyCodes.VolumeUp;

keyMap[hcap.key.Code.RED] = KeyCodes.Red;
keyMap[hcap.key.Code.GREEN] = KeyCodes.Green;
keyMap[hcap.key.Code.YELLOW] = KeyCodes.Yellow;
keyMap[hcap.key.Code.BLUE] = KeyCodes.Blue;

keyMap[461] = KeyCodes.Back;
keyMap[457] = KeyCodes.Stop;
keyMap[403] = KeyCodes.Red;
keyMap[404] = KeyCodes.Green;
keyMap[405] = KeyCodes.Yellow;
keyMap[458] = KeyCodes.Play;
keyMap[602] = KeyCodes.Menu;
keyMap[415] = KeyCodes.Play;
keyMap[413] = KeyCodes.Stop;
keyMap[19] = KeyCodes.Pause;
keyMap[417] = KeyCodes.Forward;
keyMap[412] = KeyCodes.Rewind;
keyMap[427] = KeyCodes.ChUp;
keyMap[428] = KeyCodes.ChDown;
