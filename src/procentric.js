//import Device from 'pelican';
var jQuery = require('jquery');
var Device = require('pelican-device').default;
var KeyHanlder = require('./keyhandler').default;
var Core = require('./core').default;
var debug = require('./debug').default;

export default class Procentric extends Device {
    constructor() {
        super();
        this.core = new Core(this);
        this.keyHandler = new KeyHanlder(this);
        this.media = {};
        this.duration = 0;
    }

    /**
     * Start bootstrap process
     */
    bootstrap() {
        document.addEventListener('keydown', this.keyHandler.process.bind(this.keyHandler), false);

        // if we are not on LG, bootstrap will never be ready
        if (navigator.userAgent.indexOf(';LGE ;') < 0) {
            debug.log('Procentric device not detected, bootstrap failed.');
            this.triggerDeviceFail('Procentric device not detected');
            return;
        }

        this.core.setProperty("tv_channel_attribute_floating_ui", "0");
        this.core.setProperty(hcap.property.PicturePropertyKey.ASPECT_RATIO, 2);
        this.core.executePreload();
        this.core.preloadCheck();
    }


    /**
     * Get device platform.
     * Device platform is the unique name for every platform. Eg: "DESKTOP", "ENSEO", "PROCENTRIC"
     * @return {string} Device platform
     */
    getPlatform() {
        return 'PROCENTRIC';
    }

    /**
     * Get hardware model.
     * @return {string}
     */
    getHardwareModel() {
        return this.core.model;
    }

    /**
     * Get hardware version
     * @return {string}
     */
    getHardwareVersion() {
        return '';
    }

    /**
     * Get hardware serial number
     * @return {string}
     */
    getHardwareSerial() {
        return this.core.serialNumber;
    }

    /**
     * Get middleware version
     * @return {string}
     */
    getMiddlewareVersion() {
        return this.core.platformVersion;
    }

    /**
     * Network status
     * @typedef {Device[]} NetworkStatus
     * @property {string} Device.status
     * @property {string} Device.mode "NOT_REACHABLE", "UNKNOWN", "WIRE" or "WIRELESS"
     */

    /**
     * Get all network devices
     * @return {Array}
     */
    getNetworkDevices() {
        return this.core.networkDevices;
    }

    /**
     * Get power status
     * @return {boolean}
     */
    getPower() {
        var defer = jQuery.Deferred();
        hcap.power.getPowerMode({
            "onSuccess":function(s) {
                console.log("onSuccess power mode " + s.mode);
                defer.resolve(s.mode == hcap.power.PowerMode.NORMAL);
            },
            "onFailure":function(f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
                defer.reject(f.errorMessage);
            }
        });
        return defer.promise();
    }

    /**
     * Set power status
     */
    setPower(state) {
        var defer = jQuery.Deferred();
        var mode = state ? hcap.power.PowerMode.NORMAL : hcap.power.PowerMode.WARM;
        hcap.power.setPowerMode({
            "mode": mode,
            "onSuccess": function (s) {
                console.log("onSuccess set power mode ");
                defer.resolve();
            },
            "onFailure": function (f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
                defer.reject(f.errorMessage);
            }
        });
        return defer.promise();
    }

    /**
     * Get volume level
     */
    getVolume() {
        return this.core.volume;
    }

    /**
     * Set volume level
     */
    setVolume(level) {
        this.core.setVolumeLevel(level);
    }

    /**
     * Play a tv channel
     * @param {Object} param - Parameters object
     * @param {string} param.mode - Tuning Mode, "LogicalNumber", "ChannelNumber", "Frequence", "IPAddressPort"
     * @param {string} param.type - Channel type, "IP", "Analog" or "Digital"
     * @param {string} param.rfType - RF broadcast type, "Cable" or "Air"
     * @param {number} param.majorNumber - RF channel number
     * @param {number} param.minorNumber - RF minor channel for digital signals
     * @param {string} param.ip - UDP ip address
     * @param {number} param.port - UDP port
     * @return {Object} jQuery promise
     */
    playChannel(param) {
        var self = this;
        var defer = jQuery.Deferred();
        var request = {
            onSuccess: function () {
                defer.resolve();
            },
            onFailure: function (f) {
                defer.reject(f.errorMessage);
            }
        };

        var type = (param.type || 'Analog').toLowerCase();
        var mode = (param.mode || 'ChannelNumber').toLowerCase();


        if (mode == 'logicalnumber') {
            request.logicalNumber = param.logicalNumber;
            request.channelType = hcap.channel.ChannelType.RF;
            request.rfBroadcastType = hcap.channel.RfBroadcastType.CABLE;
        }

        else {

            if (type == 'analog') {
                request.channelType = hcap.channel.ChannelType.RF;
                request.majorNumber = param.majorNumber;
                request.minorNumber = 0;
                request.rfBroadcastType = (param.rfType == 'Air') ? hcap.channel.RfBroadcastType.ANALOG_NTSC : hcap.channel.RfBroadcastType.CABLE;
            }
            else if (type == 'digital') {
                request.channelType = hcap.channel.ChannelType.RF;
                request.majorNumber = param.majorNumber;
                request.minorNumber = param.minorNumber;
                request.rfBroadcastType = (param.rfType == 'Air') ? hcap.channel.RfBroadcastType.TERRESTRIAL : hcap.channel.RfBroadcastType.CABLE;
            }
            else if (type == 'ip') {
                request.channelType = hcap.channel.ChannelType.IP;
                request.ip = param.ip;
                request.port = param.port;
                request.ipBroadcastType = hcap.channel.ipBroadcastType.UDP;
            }
        }

        hcap.channel.requestChangeCurrentChannel(request);
        return defer.promise();
    }


    /**
     * Stop current playing channel
     */
    stopChannel() {
        var self = this;
        var defer = jQuery.Deferred();

        console.log('Major Number: ' + self.core.startChannel.majorNumber);
        console.log('Minor Number: ' + self.core.startChannel.minorNumber);

        var sc = {};
        sc.mode = self.core.startChannel.mode;
        sc.rfType = self.core.startChannel.rfType;
        if(self.core.startChannel.logicalNumber){
            sc.logicalNumber = self.core.startChannel.logicalNumber;
        } else {
            sc.majorNumber = self.core.startChannel.majorNumber;
            sc.minorNuber = self.core.startChannel.minorNumber;
        }

        console.log('Procentric PlayChannel Start Channel object');
        console.log(JSON.stringify(sc));

        self.playChannel(sc)
            .done(function () {
                defer.resolve();
                console.log('In Porcentric.stopChannel we have played the Start Channel');
            });

        return defer.promise();
    }

    /**
     * Play a media stream
     * @param {Object} param - Parameter object
     * @param {string} param.url - Video URL
     * @param {string} param.mimeType - Video mime type
     */
    playMedia(param) {
        var self = this;
        var defer = jQuery.Deferred();

        param.mimeType = param.mimeType || 'video/mp4';
        param.repeatCount = param.repeatCount || 1;

        self.startMedia(param)
            .done(function () {
                self.media.play({
                    "repeatCount": param.repeatCount,
                    "onSuccess": function () {
                        defer.resolve();
                        console.log("We are starting to play media from: " + param.url);
                    },
                    "onFailure": function (f) {
                        defer.reject(f.errorMessage);
                        console.log("We are not able to play media from: " + param.url);
                        console.log("onFailure : errorMessage = " + f.errorMessage);
                    }
                });
            });

        return defer.promise();
    }

    /**
     * Play a media stream
     * @param {Object} param - Parameter object
     * @param {string} param.url - Video URL
     * @param {string} param.mimeType - Video mime type
     */
    startMedia(param) {
        var self = this;
        var defer = jQuery.Deferred();

        hcap.Media.startUp({
            "onSuccess": function () {
                console.log("We successfully initiated " + param.url);
                self.media = hcap.Media.createMedia({
                    "url": param.url,
                    "mimeType": param.mimeType
                });
                defer.resolve();
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("We were not able to initialize " + param.url);
                console.log("onFailure : errorMessage = " + f.errorMessage);
            }
        });

        return defer.promise();
    }

    /**
     * Stop current playing media
     */
    stopMedia() {
        var self = this;
        var defer = jQuery.Deferred();
        self.media.stop({
            "onSuccess": function () {
                console.log("We have stopped the media.");
                self.media.destroy({
                    "onSuccess": function () {
                        console.log("We have destroyed the media.");
                        hcap.Media.shutDown({
                            "onSuccess": function () {
                                defer.resolve();
                                console.log("We have shutdown the media.");
                            },
                            "onFailure": function (f) {
                                defer.reject(f.errorMessage);
                                console.log("onFailure : errorMessage = " + f.errorMessage);
                            }
                        });
                    },
                    "onFailure": function (f) {
                        defer.reject(f.errorMessage);
                        console.log("onFailure : errorMessage = " + f.errorMessage);
                    }
                });
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("We were not able to stop the media : errorMessage = " + f.errorMessage);
            }
        });

        return defer.promise();
    }

    /**
     * Pause current playing media
     */
    pauseMedia() {
        var defer = jQuery.Deferred();
        var self = this;

        self.media.pause({
            "onSuccess": function () {
                defer.resolve();
                console.log("We have paused the videonSuccess");
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("We failed to pause the video : errorMessage = " + f.errorMessage);
            }
        });

        return defer.promise();
    }

    /**
     * Resume current playing media
     */
    resumeMedia() {
        var defer = jQuery.Deferred();
        var self = this;

        self.media.resume({
            "onSuccess": function () {
                defer.resolve();
                console.log("We have resumed the media.");
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("We failed to resume the media : errorMessage = " + f.errorMessage);
            }
        });

        return defer.promise();
    }

    /**
     * get duration of the video from the TV.
     * duration reported ion milliseconds
     */
    getMediaDuration() {
        var self = this;
        var defer = jQuery.Deferred();

        self.media.getInformation({
            "onSuccess": function (s) {
                defer.resolve(s.contentLengthInMs);
                console.log("getMediaDuration onSuccess " + s.contentLengthInMs);
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("getDuration onFailure : errorMessage = " + f.errorMessage);
            }
        });

        return defer.promise();
    }

    /**
     * Set media position
     * @param positionInMs - position in milliseconds from the beginning
     */
    setMediaPosition(positionInMs) {
        var self = this;
        var defer = jQuery.Deferred();
        self.media.setPlayPosition({
            "positionInMs": positionInMs,
            "onSuccess": function () {
                defer.resolve();
                console.log("We are setting the Procentric media position @ " + positionInMs);
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("We failed to set the Procentric Media Position : errorMessage = " + f.errorMessage);
            }
        });
        return defer.promise();
    }

    /**
     * Get media position
     */
    getMediaPosition() {
        var self = this;
        var defer = jQuery.Deferred();
        self.media.getPlayPosition({
            "onSuccess": function (s) {
                defer.resolve(s.positionInMs);
                console.log("We are getting the Procentric media position @: " + s.positionInMs);
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                console.log("We failed to get the Procentric media position: errorMessage = " + f.errorMessage);
            }
        });
        return defer.promise();
    }

    /**
     * Set the video layer size
     * @param param object containing values
     * @param param.x - x position of video
     * @param paramy.y - y position of video
     * @param param.width - width of video
     * @param param.height - height ov video
     */
    setVideoLayerSize(param) {
        hcap.video.setVideoSize({
            "x": param.x,
            "y": param.y,
            "width": param.width,
            "height": param.height,
            "onSuccess": function () {
                debug.log("onSuccess");
            },
            "onFailure": function (f) {
                debug.log("onFailure : errorMessage = " + f.errorMessage);
            }
        });
    }

    /**
     * Get the video layer size
     */
    getVideoLayerSize() {
        debug.log('`getVideoLayerSize` is not implemented');
    }

    /**
     * Display video layer
     */
    showVideoLayer(container) {
        if (!container)
            return;
        var $container = (container instanceof jQuery) ? container : jQuery(container);
        $container.css({background: 'transparent'});
    }

    /**
     * Hide video layer
     */
    hideVideoLayer(container) {
        if (!container)
            return;
        var $container = (container instanceof jQuery) ? container : jQuery(container);
        $container.css({background: ''});
    }

    /**
     * Set local time (for devices that does not uses NTP client)
     * @param {Object} param - Parameter object
     * @param {Date} param.date
     * @param {Number} param.year
     * @param {Number} param.month
     * @param {Number} param.day
     * @param {Number} param.hour
     * @param {Number} param.minute
     * @param {Number} param.second
     * @param {Number} param.gmtOffsetInMinute
     * @param {Number} param.isDaylightSaving
     */
    setTime(param) {
        this.core.setLocalTime(param);
    }

    /**
     * Set local timezone
     */
    /**
     * Set local timezone
     * @param {string} param.name - Timezone name. eg: "Amercia/Los_Angles"
     * @param {string} param.posix - Timezone posix format. eg: "EST5EDT"
     */
    setTimezone(param) {
    }

    reload(param) {
        hcap.power.reboot({});
    }

    reboot(param) {
        hcap.power.reboot({});
    }

    setExternalInput(input) {
        var defer = jQuery.Deferred();
        var inputObj = inputMap[input];

        if (!inputObj) {
            defer.reject("Invalid input string: " + input);
        }
        else {
            debug.log('switching input to type:' + inputObj.inputType + ' index:' + inputObj.inputIndex + ' ...');
            hcap.externalinput.setCurrentExternalInput({
                "type": inputObj.inputType,
                "index": inputObj.inputIndex,
                "onSuccess": function () {
                    debug.log("Input switched successfully");
                    defer.resolve();
                },
                "onFailure": function (f) {
                    debug.log("Input switched failed: errorMessage = " + f.errorMessage);
                    defer.reject(f.errorMessage);
                }
            });
        }
        return defer.promise();
    }

    getExternalInput() {
        var defer = jQuery.Deferred();
        var input = '';
        hcap.externalinput.getCurrentExternalInput({
            "onSuccess": function (s) {
                debug.log("Current input type:" + s.type + " index:" + s.index);
                input = reverseInputMap[s.type];
                if(!input) {
                    defer.reject("Unknown input type:" + s.type);
                }
                else {
                    if(input != 'TV')
                        input += (s.index + 1);
                    defer.resolve(input);
                }
            },
            "onFailure": function (f) {
                debug.log("Failed to check current input: errorMessage = " + f.errorMessage);
                defer.reject(f.errorMessage);
            }
        });
        return defer.promise();
    }

    getStartChannel(){
        return this.core.startChannel;
    }

    setStartChannel(startChannel){
        if(startChannel) {
            this.core.startChannel = startChannel;
            console.log('Setting start channel');
            console.log(JSON.stringify(startChannel));
        }
    }
}

const inputMap = {}, reverseInputMap = {};

inputMap['TV'] = {inputType: hcap.externalinput.ExternalInputType.TV, inputIndex: 0};
inputMap['Tuner'] = {inputType: hcap.externalinput.ExternalInputType.TV, inputIndex: 0};
inputMap['HDMI'] = {inputType: hcap.externalinput.ExternalInputType.HDMI, inputIndex: 0};
inputMap['HDMI1'] = {inputType: hcap.externalinput.ExternalInputType.HDMI, inputIndex: 0};
inputMap['HDMI2'] = {inputType: hcap.externalinput.ExternalInputType.HDMI, inputIndex: 1};
inputMap['HDMI3'] = {inputType: hcap.externalinput.ExternalInputType.HDMI, inputIndex: 2};
inputMap['Component'] = {inputType: hcap.externalinput.ExternalInputType.COMPONENT, inputIndex: 0};
inputMap['Component1'] = {inputType: hcap.externalinput.ExternalInputType.COMPONENT, inputIndex: 0};
inputMap['RGB'] = {inputType: hcap.externalinput.ExternalInputType.RGB, inputIndex: 0};
inputMap['RGB1'] = {inputType: hcap.externalinput.ExternalInputType.RGB, inputIndex: 0};
inputMap['Composite'] = {inputType: hcap.externalinput.ExternalInputType.COMPOSITE, inputIndex: 0};
inputMap['Composite1'] = {inputType: hcap.externalinput.ExternalInputType.COMPOSITE, inputIndex: 0};

reverseInputMap[hcap.externalinput.ExternalInputType.TV] = 'TV';
reverseInputMap[hcap.externalinput.ExternalInputType.HDMI] = 'HDMI';
reverseInputMap[hcap.externalinput.ExternalInputType.COMPONENT] = 'Component';
reverseInputMap[hcap.externalinput.ExternalInputType.RGB] = 'RGB';
reverseInputMap[hcap.externalinput.ExternalInputType.COMPOSITE] = 'Composite';